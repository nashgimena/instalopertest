var instacomment = angular.module("instacomment", ["ngStorage"]);

instacomment.controller("CommentController", function($scope, $localStorage, $filter, $compile) {

	$scope.postCommentSection = false;
	$scope.hideCommentBox = true;
	$scope.showCommentsSection = false;
	$scope.id = 0;
	$scope.showReplyBox = false;
	$scope.hideReplyBox = true;

	$localStorage.commentsData = [];
	$scope.bdays = [];

	$scope.load = function() {
		$scope.data = $localStorage.commentsData;
	};

	$scope.replyComment = function() {
		$scope.showReplyBox = true;
		// var $a = angular.element('.comment-reply').append("<div ng-model='comment' id='froala-editor' class=''></div>");
		// $compile($a)($scope);
		// angular.element('#froala-editor').froalaEditor();
	};

	$scope.cancelReply = function() {
		$scope.showReplyBox = false;
		var $el = angular.element('.comment-reply').text('');
		$compile($el)($scope);
	};

	$scope.submitReply = function() {
		$scope.showReplyBox = false;
		var $el = angular.element('.comment-reply').text('');
		$compile($el)($scope);
		var val = angular.element('.fr-view p');
		$scope.date = new Date();
		var date = new Date();
		var utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
		$scope.dateNow = $filter('date')(utc,'MMMM d, yyyy hh:mm:ss UTC');
	};

	$scope.editComment = function() {
		$scope.postCommentSection = true;
	};

	$scope.deleteComment = function(item) {
		var index = $localStorage.commentsData.indexOf(item);
		$localStorage.commentsData.splice(index, 1);
		console.log('delete');
		console.log(item, "item");
		console.log(index);
	};

	$scope.submitComment = function() {
		// $scope.postCommentSection = false;
		$scope.hideCommentBox = true;
		var val = angular.element('.fr-view p');
		$scope.date = new Date();
		var date = new Date();
		var utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
		$scope.dateNow = $filter('date')(utc,'MMMM d, yyyy hh:mm:ss UTC');
		$localStorage.commentsData.push({
			id: $scope.id,
			comment: val[0].innerHTML,
			date: $scope.dateNow,
			reply: '0',
			showReply: '0',
		});
		$scope.bdays = $localStorage.commentsData;
		$scope.data = $localStorage.commentsData;
		console.log($scope.data);
		console.log(val[0].innerHTML);
		// var $el = angular.element('.pcs').text('');
		// $compile($el)($scope);
		$scope.id++;
	};

});