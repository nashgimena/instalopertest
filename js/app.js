var instacomment = angular.module("instacomment", ["ngStorage"]);

instacomment.controller("CommentController", function($scope, $localStorage, $filter, $compile) {

	$scope.postCommentBtn = true;			// Button for posting a comment (For showing the section of textarea)
	$scope.cancelCommentBtn = false;		// Button for cancelling posting of a comment
	$scope.postCommentSection = false;		// Section that displays text editor and submit button for posting/editing a comment
	$scope.commentsSection = false;			// Section that displays all comments/replies
	$scope.submitEditCommentBtn = false;	// Button for submitting an edited comment
	$scope.replySection = false;			// Section that displays all replies for each comment
	$scope.commentReply = 0;				// Determines if a user will post a comment. If 0, user will not post a comment. If 1, user will post a comment.
	$scope.comment = '';
	$scope.reply = '';

	$scope.id = 1;							// ID for every comment/reply. Starts at 1 because if comment has no reply, default would be 0.
	$scope.editId = 0;
	$scope.indexId = 0;
		$scope.keepGoing = false;

	$localStorage.commentsData = [];		// Localstorage for all data
	$scope.data = [];						// Instance/copy of localstorage

	// Start - Functions for posting, editing, deleting and cancelling comments //
		// OK
		$scope.postComment = function() {
			$scope.postCommentSection = true;
			$scope.submitCommentBtn = true;
			$scope.submitEditCommentBtn = false;
			$scope.postCommentBtn = false;
			$scope.cancelCommentBtn = true;
		};
		
		// OK
		$scope.cancelComment = function() {
			$scope.postCommentBtn = true;
			$scope.cancelCommentBtn = false;
			$scope.postCommentSection = false;
			$scope.commentsSection = false;
			$scope.submitCommentBtn = false;
			$scope.submitEditCommentBtn = false;
			$scope.comment = '';
			$scope.reply = '';
		};

		// OK
		$scope.submitComment = function(data) {
			$scope.postCommentSection = false;
			$scope.commentsSection = true;
			$scope.postCommentBtn = true;
			$scope.cancelCommentBtn = false;

			var date = new Date();
			var utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
			var dateNow = $filter('date')(utc,'MMMM d, yyyy hh:mm:ss UTC');
			$scope.data.push({
				id: $scope.id,
				comment: data,
				date: dateNow,
				parent: 0,
			});
			$localStorage.commentsData = $scope.data;
			$scope.id++;
			$scope.comment = ''; 
			$scope.reply = '';
		};

		// OK
		$scope.deleteComment = function(item) {
			if (confirm("Are you sure you want to delete this?")) {
				var index = $scope.data.indexOf(item);
				$scope.data.splice(index, 1);
				$localStorage.commentsData = $scope.data;
				if ( $scope.data.length === 0)
					$scope.cancelComment();
				var $col = angular.element('#keepGoing'+item.id).css('display', 'none');
				$compile($col)($scope);
			}
		};

		// OK
		$scope.editBtn = function(index, id) {
			$scope.postCommentSection = true;
			$scope.postCommentBtn = false;
			$scope.submitEditCommentBtn = true;
			$scope.submitCommentBtn = false;
			$scope.editId = id;
			$scope.indexId = index;
			if($scope.data[index].id === id)
				$scope.comment = $scope.data[index].comment;
		};

		// OK
		$scope.submitEditedComment = function(data, index, id) {
			$scope.postCommentSection = false;
			$scope.postCommentBtn = true;
			if($scope.data[index].id === id)
				$scope.data[index].comment = data;
			$localStorage.commentsData = $scope.data;
			$scope.comment = ''; 
		};
	// End - Functions for posting, editing, deleting and cancelling comments //

	// Start - Functions for posting and cancelling replies for comments //
	// OK
	$scope.replyBtn = function(id) {
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
		$scope.comment = '';
		$scope.reply = '';
		var $replyEditor = angular.element('#commentEditor'+id).append('<div class="comment-reply col-md-10 col-md-offset-1">' +
					'<textarea ng-model="reply" class="reply-editor form-control" rows="3"></textarea>' +
					'<span ng-click="submitReply(reply,'+id+')" class="submit-comment">Submit Comment Reply</span>' +
					'<span class="func-separators">&#9679;</span>' +
					'<span ng-click="cancelReply('+id+')" class="cancel-reply submit-comment">Cancel</span>' +
					'</div>');
		$compile($replyEditor)($scope);
	};

	// 
	$scope.submitReply = function(data, cid){
		var date = new Date();
		var utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
		var dateNow = $filter('date')(utc,'MMMM d, yyyy hh:mm:ss UTC');
		$scope.data.push({
			id: $scope.id,
			comment: data,
			date: dateNow,
			parent: cid,
		});
		$localStorage.commentsData = $scope.data;

		$scope.comment = ''; 
		$scope.reply = '';
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
		var index = $scope.data.length - 1;

		if ($scope.data !== []) {
			angular.forEach($scope.data, function(i) {
				if (!$scope.keepGoing) {
					if (i.parent == cid) {
						$scope.keepGoing = true;
						var $col = angular.element('#keepGoing'+cid).css('display', 'inline');
						$compile($col)($scope);
					}
				}
			});
		}

		var $re = angular.element('#comment'+cid).append("<ul class='comment-content col-md-12' id='replySection" + $scope.id + "'>"+
						"<li><div class='comment-header'>" + 
						"<img class='comment-pic' src='img/blank-profile.png'> <span class='comment-user'>Anonymous</span>  <span class='comment-date'>"+
						dateNow + "</span></div> <div class='comment-value'>" + data + " </div><div class='comment-func' ng-model='id'> "+
						"<div ng-click='commentReplyBtn(" + $scope.id + ")' class='post-reply'>Reply Reply</div><span class='func-separators'>&#9679;</span> "+
						"<div ng-click='editReplyBtn(" + index +", " + $scope.id + ")' class='edit-comment'>Edit Reply</div><span class='func-separators'>&#9679;</span> "+
						"<div ng-click='deleteReply(" + index +")' class='delete-comment'>Delete Reply</div><div id='keepGoing"+$scope.id+"' class='col'><span class='func-separators'>&#9679;</span> "+
						"<a for='commentReplies" + $scope.id + "' data-toggle='collapse' href='#commentReplies" + $scope.id + "' aria-expanded='true' aria-controls='commentReplies" + $scope.id + "'>"+
						"<i class='glyphicon glyphicon-chevron-down'></i></a></div></div><div id='commentEditor" + $scope.id + "' class='mb15'></div>"+
					"<div id='commentReplies" + $scope.id + "' class='ml15 collapse in'></div><div id='comment" + $scope.id + "' class='ml15 collapse in'></div></li></ul>");
		$compile($re)($scope);

		$scope.keepGoing = false;
		$scope.id++;
	};

	// OK
	$scope.cancelReply = function() {
		$scope.comment = ''; 
		$scope.reply = '';
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
	};
	// End - Functions for posting and cancelling replies for comments //

	// Start - Functions for editing and deleting replies for comments //
	// OK
	$scope.deleteReply = function(cindex) {
		var item = $scope.data[cindex];
		var id = $scope.data[cindex].id;
		if (confirm("Are you sure you want to delete this?")) {
			var index = $scope.data.indexOf(item);
			$scope.data.splice(index, 1);
			
			for (i in $scope.data) {
				if (id == i.parent)
				$scope.data.splice(i, 1);
			}
			$localStorage.commentsData = $scope.data;
			var $r = angular.element('#replySection'+id).remove();
			$compile($r)($scope);
			var $col = angular.element('#keepGoing'+cindex).css('display', 'none');
			$compile($col)($scope);
		}
	};
	// End - Functions for editing and deleting replies for comments //

	$scope.editReplyBtn = function(index, id) {
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
		$scope.comment = '';
		if($scope.data[index].id === id)
			$scope.reply = $scope.data[index].comment;
		var $replyEditor = angular.element('#commentEditor'+id).append('<div class="comment-reply col-md-10 col-md-offset-1">' +
					'<textarea ng-model="reply" class="reply-editor form-control" rows="3"></textarea>' +
					'<span ng-click="submitEditedCommentReply(reply,'+index+','+id+')" class="submit-comment">Submit Comment Reply</span>' +
					'<span class="func-separators">&#9679;</span>' +
					'<span ng-click="cancelReply()" class="cancel-reply submit-comment">Cancel</span>' +
					'</div>');
		$compile($replyEditor)($scope);
	};

	$scope.submitEditedCommentReply = function(data, index, id) {
		if($scope.data[index].id === id)
			$scope.data[index].comment = data;
		$localStorage.commentsData = $scope.data;
		var $replyEditor = angular.element('#replySection'+id+' li .comment-value').text(data);
		$compile($replyEditor)($scope);
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
		$scope.reply = '';

	};
	// Start - Functions for posting and cancelling replies for replies
	// OK
	$scope.commentReplyBtn = function(id) {
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
		$scope.comment = '';
		$scope.reply = '';
		var $replyEditor = angular.element('#commentEditor'+id).append('<div class="comment-reply col-md-10 col-md-offset-1">' +
					'<textarea ng-model="reply" class="reply-editor form-control" rows="3"></textarea>' +
					'<span ng-click="submitCommentReply(reply,'+id+')" class="submit-comment">Submit Reply Reply</span>' +
					'<span class="func-separators">&#9679;</span>' +
					'<span ng-click="cancelCommentReply()" class="cancel-reply submit-comment">Cancel</span>' +
					'</div>');
		$compile($replyEditor)($scope);
	};

	// 
	$scope.submitCommentReply = function(data, cid){
		var date = new Date();
		var utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
		var dateNow = $filter('date')(utc,'MMMM d, yyyy hh:mm:ss UTC');
		$scope.data.push({
			id: $scope.id,
			comment: data,
			date: dateNow,
			parent: cid,
		});
		$localStorage.commentsData = $scope.data;

		$scope.comment = ''; 
		$scope.reply = '';
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
		var index = $scope.data.length - 1;

		if ($scope.data !== []) {
			angular.forEach($scope.data, function(i) {
				if (!$scope.keepGoing) {
					if (i.parent == cid) {
						$scope.keepGoing = true;
						var $col = angular.element('#keepGoing'+cid).css('display', 'inline');
						$compile($col)($scope);
					}
				}
			});
		}

		var $re = angular.element('#commentReplies'+cid).append("<ul class='comment-content col-md-12' id='replySection" + $scope.id + "'>"+
						"<li><div class='comment-header'>" + 
						"<img class='comment-pic' src='img/blank-profile.png'> <span class='comment-user'>Anonymous</span>  <span class='comment-date'>"+
						dateNow + "</span></div> <div class='comment-value'>" + data + " </div><div class='comment-func' ng-model='id'> "+
						"<div ng-click='commentReplyBtn(" + $scope.id + ")' class='post-reply'>Reply Reply</div><span class='func-separators'>&#9679;</span> "+
						"<div ng-click='editReplyBtn(" + index +", " + $scope.id + ")' class='edit-comment'>Edit Reply</div><span class='func-separators'>&#9679;</span> "+
						"<div ng-click='deleteReply(" + index +")' class='delete-comment'>Delete Reply</div><div id='keepGoing"+$scope.id+"' class='col'><span class='func-separators'>&#9679;</span> "+
						"<a for='commentReplies" + $scope.id + "' data-toggle='collapse' href='#commentReplies" + $scope.id + "' aria-expanded='true' aria-controls='commentReplies" + $scope.id + "'>"+
						"<i class='glyphicon glyphicon-chevron-down'></i></a></div></div><div id='commentEditor" + $scope.id + "' class='mb15'></div>"+
					"<div id='commentReplies" + $scope.id + "' class='ml15 collapse in'></div><div id='comment" + $scope.id + "' class='mb15 ml15 collapse in'></div></li></ul>");
		$compile($re)($scope);
		$scope.keepGoing = false;
		$scope.id++;
	};

	// OK
	$scope.cancelCommentReply = function() {
		$scope.comment = ''; 
		$scope.reply = '';
		var $r = angular.element('.comment-reply').remove();
		$compile($r)($scope);
	};
});